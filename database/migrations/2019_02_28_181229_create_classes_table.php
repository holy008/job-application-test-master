<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('class_id');
            $table->string('class_name');
            $table->integer('teacher_id')->nullable();
//            $table->unsignedInteger('teacher_id');
//            $table->foreign('teacher_id')->references('teacher_id')->on('teachers');
            //$table->integer('student_id');
            //$table->foreign('student_id')->references('student_id')->on('students');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classes', function (Blueprint $table) {
            Schema::dropIfExists('classes');
        });
    }
}
