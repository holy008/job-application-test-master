<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'classes';
    protected $primaryKey = 'class_id';
    protected $fillable = ['class_name', 'teacher_id'];

    public function teachers()
    {
        return $this->hasOne('App\Teachers', 'teacher_id', 'teacher_id');
    }
}
