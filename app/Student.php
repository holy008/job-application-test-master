<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $primaryKey = 'student_id';
    protected $fillable = ['student_name', 'class_id'];

    public function classes()
    {
        return $this->hasOne('App\Classes', 'class_id', 'class_id');
    }
}
