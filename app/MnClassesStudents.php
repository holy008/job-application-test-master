<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MnClassesStudents extends Model
{
    protected $table = 'mn_classes_students';
    protected $primaryKey = 'id';
    protected $fillable = ['class_id', 'student_id'];

    public function classes()
    {
        return $this->hasMany('App\Classes', 'class_id', 'class_id');
    }

    public function students()
    {
        return $this->hasMany('App\Students', 'student_id', 'student_id');
    }
}
