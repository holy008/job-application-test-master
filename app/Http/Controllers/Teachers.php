<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;

class Teachers extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function list(){
        $teachers = \App\Teacher::all();
        return view('list_teachers', ['teachers' => $teachers]);
    }

    public function index()
    {
        //
    }

    public function create()
    {
        return view('new_teacher', []);
    }

    public function store(Request $request)
    {
        $request->validate([
            'teacher_name'=>'required'
        ]);

        $teacher = new Teacher([
            'teacher_name' => $request->get('teacher_name')
        ]);
        $teacher->save();
        return redirect('/teachers')->with('success', 'Teacher has been added');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $teacher = \App\Teacher::find($id);
        return view('edit_teacher', ['teacher' => $teacher]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'teacher_name'=>'required'
        ]);

        $teacher = \App\Teacher::find($id);
        $teacher->teacher_name = $request->get('teacher_name');
        $teacher->save();
        return redirect('/teachers')->with('success', 'Teacher has been updated');
    }

    public function destroy($id)
    {
        $teacher = \App\Teacher::find($id);
        $teacher->delete();

        return redirect('/teachers')->with('success', 'Teacher has been deleted');
    }
}
