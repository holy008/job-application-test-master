<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class Classes extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function list(){
        $classes = \App\Classes::all();
        return view('list_classes', ['classes' => $classes]);
    }

    public function index()
    {
        //
    }

    public function create()
    {
        $teachers = \App\Teacher::all();

        return view('new_class', ['teachers' => $teachers]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'class_name'=>'required'
        ]);

        $class = new \App\Classes([
            'class_name' => $request->get('class_name'),
            'teacher_id' => $request->get('teacher')
        ]);
        $class->save();
        return redirect('/classes')->with('success', 'Class has been added');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $class = \App\Classes::find($id);
        $teachers = \App\Teacher::all();
        return view('edit_class', ['class' => $class,
                                         'teachers' => $teachers]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'class_name'=>'required'
        ]);

        $class = \App\Classes::find($id);
        $class->class_name = $request->get('class_name');
        $class->teacher_id = $request->get('teacher');
        $class->save();
        return redirect('/classes')->with('success', 'Class has been updated');
    }

    public function destroy($id)
    {
        $class = \App\Classes::find($id);
        $class->delete();

        return redirect('/classes')->with('success', 'Class has been deleted');
    }

    public function export_pdf(){
        $classes = \App\Classes::get();
        foreach($classes as $class){
            $teachers = \App\Teacher::where('teacher_id', $class->teacher_id)->get();
            $class->teachers = $teachers;

            $students = \App\Student::where('class_id', $class->class_id)->get();
            $class->students = $students;
        }
        $pdf = PDF::loadView('classes_pdf', ['classes' => $classes]);
        //$pdf->save(storage_path().'_filename.pdf');
        return $pdf->download('classes.pdf');
    }
}
