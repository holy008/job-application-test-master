<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Classroom extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function list(){
        $classes = \App\Classes::all();
        return view('list_classroom', ['classes' => $classes]);
    }

    public function index()
    {
        //
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $class = \App\Classes::find($id);
        $teachers = \App\Teacher::all();
        $selectedTeacher = [];
        $countSelectedTeacher = \App\MnClassesTeachers::where('class_id', $id)->count();
        if($countSelectedTeacher > 0){
            $selectedTeacher = \App\MnClassesTeachers::where('class_id', $id)->first();
        }
        $students = \App\Student::all();
        $selectedStudents = \App\MnClassesStudents::where('class_id', $id)->get();
        return view('new_classroom', ['class' => $class,
                                            'teachers' => $teachers,
                                            'selectedTeacher' => $selectedTeacher,
                                            'countSelectedTeacher' => $countSelectedTeacher,
                                            'students' => $students,
                                            'selectedStudents' => $selectedStudents]);
    }

    public function update(Request $request, $id)
    {
        $delTeacher = \App\MnClassesTeachers::where('class_id', $request->get('class'))->first();
        if(\App\MnClassesTeachers::where('class_id', $request->get('class'))->count() > 0){
            $delTeacher->delete();
        }

        $delStudents = \App\MnClassesStudents::where('class_id', $request->get('class'))->get();
        foreach($delStudents as $delStudent){
            $delStudent->delete();
        }

        $mnClassesTeachers = new \App\MnClassesTeachers([
            'class_id' => $request->get('class'),
            'teacher_id' => $request->get('teacher')
        ]);
        $mnClassesTeachers->save();

        $students = $request->students;
        if($students != null){
            foreach ($students as $student_id) {
                $mnClassesStudents = new \App\MnClassesStudents([
                    'class_id' => $request->get('class'),
                    'student_id' => $student_id
                ]);
                $mnClassesStudents->save();
            }
        }

        return redirect('/classrooms')->with('success', 'Classroom has been added');
    }

    public function destroy($id)
    {

    }
}
