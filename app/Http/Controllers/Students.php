<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class Students extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function list(){
        $students = \App\Student::all();
        return view('list_students', ['students' => $students]);
    }

    public function index()
    {
        //
    }

    public function create()
    {
        $classes = \App\Classes::all();
        return view('new_student', ['classes' => $classes]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'student_name'=>'required'
        ]);

        $student = new Student([
            'student_name' => $request->get('student_name'),
            'class_id' => $request->get('class'),
        ]);
        $student->save();
        return redirect('/students')->with('success', 'Student has been added');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $student = \App\Student::find($id);
        $classes = \App\Classes::all();
        return view('edit_student', ['student' => $student,
                                           'classes' => $classes]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'student_name'=>'required'
        ]);

        $student = \App\Student::find($id);
        $student->student_name = $request->get('student_name');
        $student->class_id = $request->get('class');
        $student->save();
        return redirect('/students')->with('success', 'Student has been updated');
    }

    public function destroy($id)
    {
        $student = \App\Student::find($id);
        $student->delete();

        return redirect('/students')->with('success', 'Student has been deleted');
    }
}
