<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MnClassesTeachers extends Model
{
    protected $table = 'mn_classes_teachers';
    protected $primaryKey = 'id';
    protected $fillable = ['class_id', 'teacher_id'];

    public function classes()
    {
        return $this->hasMany('App\Classes', 'class_id', 'class_id');
    }

    public function teachers()
    {
        return $this->hasMany('App\Teachers', 'teacher_id', 'teacher_id');
    }
}
