<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <h1>Classes List</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Class Name</th>
            <th>Teacher</th>
            <th>Student</th>
        </tr>
        </thead>
        <tbody>
        @foreach($classes as $class)
            <tr>
                <td>{{ $class->class_id }}</td>
                <td>{{ $class->class_name }}</td>
                <td>
                    <?php $count = 0; ?>
                    @foreach($class->teachers as $teacher)
                        <?php $count++; ?>
                        @if($count == 1)
                            {{''}}
                        @else
                            {{', '}}
                        @endif
                        {{$teacher->teacher_name}}
                    @endforeach
                    @if($count == 0)
                        {{'-'}}
                    @endif
                </td>
                <td>
                    <?php $count = 0; ?>
                    @foreach($class->students as $student)
                        <?php $count++; ?>
                        @if($count == 1)
                            {{''}}
                        @else
                            {{', '}}
                        @endif
                        {{$student->student_name}}
                    @endforeach
                    @if($count == 0)
                        {{'-'}}
                    @endif
                </td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>