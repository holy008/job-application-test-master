@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        List Classes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="btn btn-success btn-sm" href="{{ URL::to('/classes_export_pdf') }}">Export PDF</a>
                        <br>
                        <a href="{{ route('classes', [])  }}" class="btn btn-default"> List </a>
                        <a href="{{ route('new_class', [])  }}" class="btn btn-default"> New </a>
                        <div>

                            <div class="card-body">
                                <table class="table table-striped">
                                    <tr>
                                        <th>CLASS ID</th>
                                        <th>CLASS NAME</th>
                                        <th>ACTION</th>
                                    </tr>
                                    @foreach ($classes as $class)
                                        <tr>
                                            <td>{{ $class->class_id }}</td>
                                            <td>{{ $class->class_name }}</td>
                                            <td>
                                                <a href="{{ route('edit_class', $class->class_id)  }}" class="btn btn-warning"> Edit </a>
                                                <a href="{{ route('delete_class', $class->class_id)  }}" class="btn btn-danger"> Delete </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
