@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Edit Class
                        <br>
                        <a href="{{ URL::route('classes', [])  }}" class="btn btn-default"> List </a>
                        <div>

                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form method="post" action="{{ route('update_class', $class->class_id) }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="class_id" value="{{ $class->class_id }}"/>
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="class_name" value="{{ $class->class_name }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Teacher:</label>
                                        <select class="form-control" name="teacher" id="teacher">
                                            <option disabled selected value> -- select an option -- </option>
                                            @foreach($teachers as $teacher){
                                            <option value="{{$teacher->teacher_id}}" <?= ($class->teacher_id == $teacher->teacher_id ? "selected" : "") ?>>{{$teacher->teacher_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
