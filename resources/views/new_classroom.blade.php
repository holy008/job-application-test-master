@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Edit Classroom
                        <br>
                        <a href="{{ URL::route('classrooms', [])  }}" class="btn btn-default"> List </a>
                        <div>

                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form method="post" action="{{ route('update_classroom', 0) }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Class:</label>
                                        <select class="form-control" name="class" id="class">
                                            <option value="{{$class->class_id}}">{{$class->class_name}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Teacher:</label>
                                        <select class="form-control" name="teacher" id="teacher">
                                            @foreach($teachers as $teacher){
                                                @if ($countSelectedTeacher > 0)
                                                    <option value="{{$teacher->teacher_id}}" <?= ($selectedTeacher->teacher_id == $teacher->teacher_id ? "selected" : "") ?>>{{$teacher->teacher_name}}</option>
                                                @else
                                                <option value="{{$teacher->teacher_id}}">{{$teacher->teacher_name}}</option>
                                                 @endif
                                            @endforeach
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        {{--<label for="name">Student:</label>--}}
                                        {{--<select class="form-control" name="student" id="student">--}}
                                            {{--@foreach($students as $student){--}}
                                            {{--<option value="{{$student->student_id}}">{{$student->student_name}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                        <?php
                                            $arrSelectedStudents = array();
                                            foreach($selectedStudents as $stud){
                                                array_push($arrSelectedStudents, $stud->student_id);
                                            }
                                        ?>
                                        <label for="name">Student:</label>
                                        @foreach($students as $student)
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="students[]" value="{{ $student->student_id }}" <?= (in_array($student->student_id, $arrSelectedStudents) == true ? "checked" : "") ?>>
                                                <label class="form-check-label">
                                                    {{$student->student_name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
