@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Edit Student
                        <br>
                        <a href="{{ URL::route('students', [])  }}" class="btn btn-default"> List </a>
                        <div>

                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form method="post" action="{{ route('update_student', $student->student_id) }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="student_id" value="{{ $student->student_id }}"/>
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="student_name" value="{{ $student->student_name }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Class:</label>
                                        <select class="form-control" name="class" id="class">
                                            <option disabled selected value> -- select an option -- </option>
                                            @foreach($classes as $class){
                                            <option value="{{$class->class_id}}" <?= ($class->class_id == $student->class_id ? "selected" : "") ?>>{{$class->class_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
