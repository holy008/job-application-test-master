@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        List Classrooms &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="btn btn-success" href="{{ URL::to('/customers/pdf') }}">Export PDF</a>
                        {{--<a href="{{ route('classrooms', [])  }}" class="btn btn-default"> List </a>--}}
                        {{--<a href="{{ route('new_classroom', [])  }}" class="btn btn-default"> New </a>--}}
                        <div>

                            <div class="card-body">
                                <table class="table table-striped">
                                    <tr>
                                        <th>CLASS NAME</th>
                                        <th>ACTION</th>
                                    </tr>
                                    @foreach ($classes as $class)
                                        <tr>
                                            <td>{{ $class->class_name }}</td>
                                            <td>
                                                <a href="{{ route('edit_classroom', $class->class_id)  }}" class="btn btn-warning"> Edit </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
