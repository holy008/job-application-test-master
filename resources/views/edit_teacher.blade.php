@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Edit Teacher
                        <br>
                        <a href="{{ URL::route('teachers', [])  }}" class="btn btn-default"> List </a>
                        <div>

                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form method="post" action="{{ route('update_teacher', $teacher->teacher_id) }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="teacher_id" value="{{ $teacher->teacher_id }}"/>
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="teacher_name" value="{{ $teacher->teacher_name }}"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
