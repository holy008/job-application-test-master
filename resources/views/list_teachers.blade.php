@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        List Teachers
                        <br>
                        <a href="{{ route('teachers', [])  }}" class="btn btn-default"> List </a>
                        <a href="{{ route('new_teacher', [])  }}" class="btn btn-default"> New </a>
                        <div>

                            <div class="card-body">
                                <table class="table table-striped">
                                    <tr>
                                        <th>TEACHER ID</th>
                                        <th>TEACHER NAME</th>
                                        <th>ACTION</th>
                                    </tr>
                                    @foreach ($teachers as $teacher)
                                        <tr>
                                            <td>{{ $teacher->teacher_id }}</td>
                                            <td>{{ $teacher->teacher_name }}</td>
                                            <td>
                                                <a href="{{ route('edit_teacher', $teacher->teacher_id)  }}" class="btn btn-warning"> Edit </a>
                                                <a href="{{ route('delete_teacher', $teacher->teacher_id)  }}" class="btn btn-danger"> Delete </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
