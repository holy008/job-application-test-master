@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        List Students
                        <br>
                        <a href="{{ route('students', [])  }}" class="btn btn-default"> List </a>
                        <a href="{{ route('new_student', [])  }}" class="btn btn-default"> New </a>
                    <div>

                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>STUDENT ID</th>
                                <th>STUDENT NAME</th>
                                <th>ACTION</th>
                            </tr>
                            @foreach ($students as $student)
                                <tr>
                                    <td>{{ $student->student_id }}</td>
                                    <td>{{ $student->student_name }}</td>
                                    <td>
                                        <a href="{{ route('edit_student', $student->student_id)  }}" class="btn btn-warning"> Edit </a>
                                        <a href="{{ route('delete_student', $student->student_id)  }}" class="btn btn-danger"> Delete </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
