<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return redirect('/login');
});

Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/students', 'Students@list')->name('students');
Route::get('/new_student', 'Students@create')->name('new_student');
Route::post('/store_student', 'Students@store')->name('store_student');
Route::get('/edit_student/{id}', 'Students@edit')->name('edit_student');
Route::post('/update_student/{id}', 'Students@update')->name('update_student');
Route::get('/delete_student/{id}', 'Students@destroy')->name('delete_student');

Route::get('/teachers', 'Teachers@list')->name('teachers');
Route::get('/new_teacher', 'Teachers@create')->name('new_teacher');
Route::post('/store_teacher', 'Teachers@store')->name('store_teacher');
Route::get('/edit_teacher/{id}', 'Teachers@edit')->name('edit_teacher');
Route::post('/update_teacher/{id}', 'Teachers@update')->name('update_teacher');
Route::get('/delete_teacher/{id}', 'Teachers@destroy')->name('delete_teacher');

Route::get('/classes', 'Classes@list')->name('classes');
Route::get('/new_class', 'Classes@create')->name('new_class');
Route::post('/store_class', 'Classes@store')->name('store_class');
Route::get('/edit_class/{id}', 'Classes@edit')->name('edit_class');
Route::post('/update_class/{id}', 'Classes@update')->name('update_class');
Route::get('/delete_class/{id}', 'Classes@destroy')->name('delete_class');
Route::get('/classes_export_pdf','Classes@export_pdf')->name('export_pdf');

Route::get('/classrooms', 'Classroom@list')->name('classrooms');
Route::get('/new_classroom', 'Classroom@create')->name('new_classroom');
Route::post('/store_classroom', 'Classroom@store')->name('store_classroom');
Route::get('/edit_classroom/{id}', 'Classroom@edit')->name('edit_classroom');
Route::post('/update_classroom/{id}', 'Classroom@update')->name('update_classroom');
Route::get('/delete_classroom/{id}', 'Classroom@destroy')->name('delete_classroom');

